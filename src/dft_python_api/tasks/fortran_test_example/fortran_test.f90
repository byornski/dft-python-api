module fortran_test

  ! Some simple fortran examples to be used with the fortran_task.py script
  ! Compile by running make in this folder (with correct mpi and python loaded)
  ! Requires f2py to compile!
  use mpi_f08
  implicit none

  ! Mark everything private except functions we directly expose
  private

  public :: add_fort, comm_info_fort

contains


  ! Add two numbers but in fortran
  subroutine add_fort(ans, a, b)
    implicit none
    integer, intent(out) :: ans
    integer, intent(in)  :: a, b
    ans = a + b
  end subroutine add_fort

  ! Inquite about the mpi commicator size and return the result
  subroutine comm_info_fort(mpi_size, mpi_comm_handle)
    implicit none
    integer, intent(out) :: mpi_size
    integer, intent(in)  :: mpi_comm_handle

    ! Example of how to set the f08 mpi objects
    !    alternatively just use mpi_comm_handle directly as the
    !    communicator with the old bindings
    type(MPI_Comm)       :: comm_world ! To hold the local world communicator
    comm_world%MPI_VAL = mpi_comm_handle

    ! Do the actual mpi call
    call MPI_COMM_SIZE(comm_world, mpi_size)

  end subroutine comm_info_fort

end module fortran_test
