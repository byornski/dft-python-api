"""IO operations for worker.pyp"""
import sys
import pickle
import os
from ..socket_io.socket import IOSocket

class ParentIO:
    """Class that handles receiving and communicating data to the parent python instance"""
    def __init__(self, comm):
        """Initialises a reader for io operations to the master process"""
        self.comm = comm
        self.rank = comm.Get_rank()
        self.isroot = self.rank == 0

        if self.isroot:
            addr, port = sys.argv[-2:]
            addr = sys.argv[1]
            port = int(sys.argv[2])
            self.socket = IOSocket()
            self.socket.connect(addr,port)

    def _recv(self):
        """Read pickled object from stdin only on the root node"""
        if self.isroot:
            data = self.socket.recv()
        else:
            data = None
        return data

    def recv_all(self):
        """Read pickled object from stdin and broadcast to all nodes"""
        return self.comm.bcast(self._recv())

    def send(self, data):
        """Send data to master process from root node"""
        if self.isroot:
            self.socket.send(data)
