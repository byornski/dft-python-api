"""Classes to control communication with parallel subprocesses"""
import os
import sys
import signal
import psutil
import pickle
import time
import atexit
import threading
from subprocess import Popen, PIPE
from pathlib import Path
from queue import Queue

from ..utils.controlflags import ControlFlag
from . import runner
from ..socket_io.socket import IOSocket

class WorkerDiedException(Exception):
    pass

class WorkerCrashed(Exception):
    """Indicates that a worker has crashed"""


def get_default_worker_script():
    """Get the default worker.py script"""
    return "-mdft_python_api.worker.worker"

# Cleanup code
DEBUG_CLEANUP=False

def cleanup_process(process: psutil.Process):
    """Terminate process and all children (ie mpirun children)"""
    if DEBUG_CLEANUP:
        print("killing process", process)

    # Gather child pids to kill
    try:
        children = list(process.children(recursive=True))
    except psutil.NoSuchProcess:
        children = []

    try:
        process.kill()
    except psutil.NoSuchProcess:
        pass

    # Kill children (ie worker threads that may have a different pgid )
    for c in children:
        if DEBUG_CLEANUP:
            print("killing child", c)
        c.kill()
    
def cleanup_file(pipe_file):
    """Delete a file"""
    try:
        if DEBUG_CLEANUP:
            print("Removing pipe", pipe_file)
        os.remove(pipe_file)
    except FileNotFoundError:
        pass # Ignore if already deleted

class Worker:
    """Represents a collection of workers of variable sizes"""
    def __init__(self, name, max_cores, parallel_runner=None, single_worker_mode=True):
        self.name = name
        self.max_cores = max_cores
        self.used_cores = 0
        self.current_worker_id = 0
        self.workers = dict()

        if parallel_runner:
            self.parallel_runner = parallel_runner
        else:
            self.parallel_runner = runner.DEFAULT_RUNNER

        self.work_queue = Queue()
        self.results_queue = Queue()

        if single_worker_mode:
            self.add_workers(max_cores)

    def add_workers(self, num_cores_per_worker, num_workers=1):
        """Add more workers to the worker pool"""
        if self.used_cores + num_workers * num_cores_per_worker > self.max_cores:
            raise ValueError("Attempted to spawn more worker threads than max_cores!")
        self.used_cores += num_workers * num_cores_per_worker

        for _ in range(num_workers):
            worker_id = f"{self.name}-{self.get_next_id()}"
            worker = WorkerIO(worker_id=worker_id,
                        num_cores=num_cores_per_worker,
                        work_queue=self.work_queue,
                        results_queue=self.results_queue,
                        parallel_runner=self.parallel_runner
            )
            self.workers[worker_id] = worker

    def add_task(self, task_id, task):
        """Adds a task to the queue"""
        self.work_queue.put((task_id, task))

    def get_next_id(self):
        """Get the next free worker id"""
        self.current_worker_id += 1
        return self.current_worker_id


    def kill(self):
        """Kills all subprocesses"""
        for worker in self.workers.values():
            worker.kill()

    def finish(self):
        """Sends the polite finish message to all workers"""
        assert self.work_queue.empty(), "Work queue wasn't empty!"
        for worker in self.workers.values():
            worker.finish()

    def get_next_finished(self):
        """Return the next finished task"""
        return_value = self.results_queue.get()

        task_id, result, *error_info = return_value

        if result == ControlFlag.WORKER_CRASHED:
            error_task, worker_id = error_info
            if hasattr(error_task, "get_error"):
                err_msg = error_task.get_error(worker_id)
                raise WorkerDiedException(f"Worker died on task with message:\n{error_task}\n{err_msg}")
            else:
                raise WorkerDiedException(f"Worker died on task {task_id}")
        else:
            return task_id, result


    def get_next_result(self):
        """Return the next finished task which returns a result"""
        result = None
        while result is None:
            task_id, result = self.get_next_finished()
        return task_id, result

class WorkerIO:
    """Represents an individual worker"""
    def __init__(self, worker_id, num_cores, work_queue, results_queue, parallel_runner:runner.ParallelRunner, worker_py=None):
        """Initialise a worker thread"""
        if worker_py is None:
            worker_py = get_default_worker_script()

        # Use current python executable
        py_exe = sys.executable

        self.num_cores = num_cores

        self.worker_id = worker_id
        self.work_queue = work_queue
        self.results_queue = results_queue


        # Make a socket for this process to connect to
        self.socket = IOSocket()
        addr, port = self.socket.listen()

        # Get the parallel command from the runner
        self.process_cmd = parallel_runner.get_run_cmd(py_exe, num_cores, args=(worker_py, addr, port))

        # Start the process
        self.process = Popen(self.process_cmd, shell=False,
                             stdin=PIPE, bufsize=0)   # PIPE with no buffering

        # Immediately add to kill at exit list
        atexit.register(cleanup_process, psutil.Process(self.process.pid))

        # Wait for socket connection
        self.socket.wait_for_connection()
        
        # Send worker id
        self.send(self.worker_id)         # Send worker id

        # Go into work loop
        self.thread = threading.Thread(target=self.work_loop,
                                       name=f"worker-{worker_id}",
                                       daemon=True)
        self.thread.start()

    def work_loop(self):
        """Take items from work queue, process them and send results to result_queue"""
        while True:
            task_id, task = self.work_queue.get()
            try:
                self.send(task)
            except BrokenPipeError:
                print(f"Died sending data: {task_id, task}")
                self.results_queue.put((task_id, ControlFlag.WORKER_CRASHED, task, self.worker_id))
                break

            if task == ControlFlag.END:
                break

            try:
                result = self.read()
            except (BrokenPipeError, EOFError):
                print(f"Died while waiting on response for task: {task_id, task}")
                self.results_queue.put((task_id, ControlFlag.WORKER_CRASHED, task, self.worker_id))
                break

            self.results_queue.put((task_id, result))
            self.work_queue.task_done()



    def send(self, data: object):
        """Send object to child process"""
        self.socket.send(data)

    def read(self) -> object:
        """Read object from child process synchronously"""
        return self.socket.recv()


    def __repr__(self):
        return "Worker(id={0.worker_id}, num_cores={0.num_cores})".format(self)


    def finish(self):
        """Polite message to thread and subprocess to close"""
        self.work_queue.put((-1, ControlFlag.END))

    def kill(self):
        """Send kill signal to subprocess"""

        # First enumerate children
        parent = psutil.Process(self.process.pid)
        children = list(parent.children(recursive=True))

        # Send kill to mpirun
        self.process.kill()

        # Kill children (ie worker threads that may have a different pgid )
        for c in children:
            c.kill()

        time.sleep(0.1)

        try:
            os.remove(self.fname)
        except FileNotFoundError:
            pass # Ignore if already deleted
