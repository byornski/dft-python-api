"""This file defines parallel runner commands (eg mpirun, srun) and allows the user to modify them. """
import os

class ParallelRunner:
    DEFAULT_PARALLEL_EXE = None
    DEFAULT_PARALLEL_CMD = ["{PARALLEL_EXE}", "-np", "{num_cores}", "{program}", "{args}"]

    def __init__(self, parallel_exe=None, parallel_cmd=None):
        self.parallel_exe = parallel_exe if parallel_exe else self.DEFAULT_PARALLEL_EXE
        self.parallel_cmd = parallel_cmd if parallel_cmd else self.DEFAULT_PARALLEL_CMD

    def get_run_cmd(self, program, num_cores, args=None):
        param_dict = {
            'PARALLEL_EXE': self.parallel_exe,
            'program': program,
            'num_cores': num_cores,
        }

        if args:
            param_dict['args'] = args

        def fmt(s):
            return [str(s).format(**param_dict)]

        cmd = []
        for c in self.parallel_cmd:
            if c == "{args}":
                for a in args:
                    cmd += fmt(a)
            else:
                cmd += fmt(c)
        return cmd
       
class CustomRunner(ParallelRunner):
    """    
    Dynamically a new runner for some custom system that I haven't included. The following
    would define the mpirun command:
    CustomRunner(["mpirun","-np","{num_cores}","{program}","{args}"])
    """
    def __init__(self, parallel_cmd):
        self.parallel_cmd = parallel_cmd
    
class SerialRunner(ParallelRunner):
    """Ignores parallel options and runs in serial"""
    DEFAULT_PARALLEL_CMD = ["{program}", "{args}"]

class SrunRunner(ParallelRunner):
    """srun -n <num_cores> --exclusive <cmd> <script>"""
    DEFAULT_PARALLEL_EXE = "srun"
    DEFAULT_PARALLEL_CMD = ["{PARALLEL_EXE}", "-n", "{num_cores}", "--exclusive", "{program}", "{args}"]

class MpirunRunner(ParallelRunner):
    """mpirun -np <num_cores> <cmd> <script>"""
    DEFAULT_PARALLEL_EXE = "mpirun"
    DEFAULT_PARALLEL_CMD = ["{PARALLEL_EXE}", "-np", "{num_cores}", "{program}", "{args}"]

# Exported list of parallel runners
PARALLEL_RUNNERS = {
    'serial': SerialRunner(),
    'mpirun': MpirunRunner(),
    'mpiexec': MpirunRunner(parallel_cmd="mpiexec"),
    'mpiexec.hydra': MpirunRunner(parallel_cmd="mpiexec.hydra"),
    'srun': SrunRunner(), 
    'custom': CustomRunner  # SPECIAL CASE -- NOT AN INSTANCE
}

ENV_RUNNER = os.environ.get("DFT_API_RUNNER")
if ENV_RUNNER is not None:
    try:
        DEFAULT_RUNNER = PARALLEL_RUNNERS[ENV_RUNNER]
    except KeyError:
        print(f"Runner \"{ENV_RUNNER}\" is not a valid option for enviromental variable DFT_API_RUNNER. Valid options are:")
        for k in PARALLEL_RUNNERS:
            print(k)
        raise Exception("Invalid choice of DFT_API_RUNNER")
else:
    DEFAULT_RUNNER = PARALLEL_RUNNERS['mpirun']
