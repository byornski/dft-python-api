import socket
import pickle


class IOSocket():
    def __init__(self):
        self.socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)

    def listen(self):
        self.socket.bind(("",0))
        self.socket.listen(1)   
        port = self.socket.getsockname()[1]
        addr = socket.gethostname()
        return addr, port

    def wait_for_connection(self):
        conn_socket,_ = self.socket.accept()
        self.socket.close()
        self.socket = conn_socket
        self.set_keepalive()

    def connect(self, addr, port):
        self.socket.connect((addr, port)) 
        self.set_keepalive()

    def send(self, data):
        sfile = self.socket.makefile('wb')
        try:
            pickle.dump(data, sfile)
        finally:
            sfile.close()

    def recv(self):
        sfile = self.socket.makefile('rb')
        try:
            data = pickle.load(sfile)
        finally:
            sfile.close()
        return data

    def set_keepalive(self):
        self.socket.setsockopt(socket.SOL_SOCKET, socket.SO_KEEPALIVE, 1)
        self.socket.setsockopt(socket.IPPROTO_TCP, socket.TCP_KEEPIDLE, 600)  # Keep alive if idle for 10 mins
        self.socket.setsockopt(socket.IPPROTO_TCP, socket.TCP_KEEPINTVL, 30) # Send keepalive packet every 30s
        self.socket.setsockopt(socket.IPPROTO_TCP, socket.TCP_KEEPCNT, 5)    # Stop after 5 failed attempts
