import numpy as np
from ase import Atoms
from ase.units import Bohr, GPa, eV
from dft_python_api import DFT_Calculator, CastepTask

calc = DFT_Calculator(calculator_type=CastepTask, debug=False, num_mpi_processes=4)
calc.set(iprint=1, max_scf_cycles=100, elec_energy_tol=1e-8, cutoff_energy="400 eV")

from ase.build import bulk

molecule = bulk('Au', 'sc', a=3.0, cubic=True)
molecule.calc = calc

print(f"Number of cores: {calc.num_processes}")
print("Energy (eV): ", molecule.get_potential_energy()/ eV)

# molecule.copy
forces1 = molecule.get_forces()
forces2 = calc.calculate_numerical_forces(molecule)

print("Forces (eV/Bohr):")
print("analytic\n", forces1/(eV/Bohr))
print("numeric \n", forces2/(eV/Bohr))
print()

print("Energy (eV): ", molecule.get_potential_energy()/ eV)

stress1 = molecule.get_stress()
stress2 = calc.calculate_numerical_stress(molecule)
stress3 = molecule.get_stress()

print("Stress (GPa):")
print("analytic\n", stress1/GPa)
print("numeric \n", stress2/GPa)
print("analytic\n", stress3/GPa)

print("Energy (eV): ", molecule.get_potential_energy()/ eV)