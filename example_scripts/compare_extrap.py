#!/usr/bin/env python3
"""Main entry point for project"""
import atexit
from ase import Atoms
from ase.data import chemical_symbols
from dft_python_api import CastepTask as dft
from dft_python_api import CastepDebugTask as dft2
from dft_python_api import Worker, units

atom_no = 7
positions_min, positions_max, num_steps = 1.0, 4.0, 20

step_size = (positions_max - positions_min) / num_steps

# Generate some tasks to do
tasks = {}
for step in range(num_steps+1):
    x = positions_min + step * step_size

    cell_data = Atoms(
        "HeHe",
        positions=[(0,0,0), (x, 0, 0)],
        cell=[10,10,10]
    )

    param_data = dft.get_default_parameters(
        "energy", cell_data, cutoff_energy = "200 eV", max_scf_cycles = 100
    )

    tasks[step] = (x*units.bohr, cell_data, param_data)

# Spawn worker
w1 = Worker("w1", 1)
w2 = Worker("w2", 1)

_, init_cell, init_param = tasks[0]

# Start worker

# Run a set of calculations
for task_id, (sep, atoms, params) in tasks.items():

    if task_id == 0:
        w1.add_task(0, dft(dft.init, atoms, params))
        w2.add_task(0, dft(dft.init, atoms, params))
    else:
        w1.add_task(task_id, dft(dft.update_atoms, atoms))    
        w2.add_task(task_id, dft(dft.update_atoms_extrap, atoms))
        
    w1.add_task(task_id, dft(dft.calculate))
    w2.add_task(task_id, dft(dft.calculate))

    # Wait for these to finish. You can also do additional
    # work on the main script while you wait for the results
    task_id, result1 = w1.get_next_result()
    task_id, result2 = w2.get_next_result()
    e1 = result1.energy
    e2 = result2.energy
    
    
    print(f"Step: {task_id:2d}\t\t Separation:{sep:.2f}\t Clean Energy: {e1.to(units.eV):.5f}\tExtrap Energy: {e2.to(units.eV):.5f}")



# Close workers
w1.add_task(0, dft(dft.finalise))
w2.add_task(0, dft(dft.finalise))
w1.get_next_finished()
w2.get_next_finished()
w1.finish()
w2.finish()
