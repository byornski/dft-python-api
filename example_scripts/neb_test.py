from ase.io import read
from ase.constraints import FixAtoms
from ase.calculators.emt import EMT
from ase.neb import NEB
from ase.optimize import BFGS
from dft_python_api import DFT_Calculator, CastepTask

initial = read('initial.traj')
final = read('final.traj')

constraint = FixAtoms(mask=[atom.tag > 1 for atom in initial])
calculator_params = {"cutoff_energy": "300 eV"}

images = [initial]
for i in range(3):
    image = initial.copy()
    image.calc = DFT_Calculator(calculator_type=CastepTask, label=f"neb{i}", num_mpi_processes=2)
    image.calc.set(**calculator_params)
    image.set_constraint(constraint)
    images.append(image)

images.append(final)

neb = NEB(images, parallel=True)
neb.interpolate()
qn = BFGS(neb, trajectory='neb.traj')
qn.run(fmax=0.05)
