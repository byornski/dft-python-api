#!/usr/bin/env python3
"""Main entry point for project"""
import atexit

from dft_python_api.tasks.fortran_example_task import FortTask
from dft_python_api import Worker

# Generate some tasks to do
num_steps = 5
tasks = {}
for step in range(num_steps):
    a = step
    b = 4 * step
    tasks[step] = (a, b)



# Spawn worker
worker_comm_size = 5
workers = Worker(worker_comm_size)

workers.add_task(100, FortTask(FortTask.comm_info))
task_id, comm_info = workers.get_next_result()
print(f"Comm size (from fortran library): {comm_info}")

# Run a set of calculations
for task_id, (a, b) in tasks.items():

    # Send the numbers off to be added
    workers.add_task(task_id, FortTask(FortTask.add, a, b))

    # Wait for these to finish. You can also do additional
    # work on the main script while you wait for the results
    task_id, result = workers.get_next_result()

    print(f"Step: {task_id} \t The sum of {a} and {b} is {result} via fortran module")



# Close workers
workers.finish()
