#!/usr/bin/env python3
"""Main entry point for project"""
import atexit
from ase import Atoms
from ase.data import chemical_symbols
from dft_python_api import Worker, units
from dft_python_api import CastepTask as dft

atom_no = 2
positions_min = 1.0*units.bohr
positions_max = 4.0*units.bohr
num_steps = 5

step_size = (positions_max - positions_min) / (num_steps-1)

# Generate some tasks to do
tasks = {}
param_data = dft.get_default_parameters(
    "energy", cutoff_energy = "100 eV"
)

for step in range(num_steps):
    x = positions_min + step * step_size

    cell_data = Atoms(
        chemical_symbols[atom_no]*2,
        positions=[(0,0,0), (units.mag(x, units.bohr), 0, 0)],
        cell=[10,10,10]
    )

    tasks[step] = (x, cell_data)

# Spawn worker
workers = Worker("main", 1)

# Run a set of calculations
for task_id, (sep, atoms) in tasks.items():
    if task_id==0:
        # If this is the first task, initialise
        workers.add_task(task_id, dft(dft.init, atoms, param_data))
    else:
        # Otherwise, extrapolate
        workers.add_task(task_id, dft(dft.update_atoms, atoms))

    # Calculate ground state energy
    workers.add_task(task_id, dft(dft.calculate))

    # Wait for these to finish. You can also do additional
    # work on the main script while you wait for the results
    task_id, result = workers.get_next_result()

    print(f"Step: {task_id} \tSeparation: {sep:.2f} \t Energy: {result.energy.to(units.eV)}")

# Close workers
workers.add_task(0, dft(dft.finalise))
workers.get_next_finished()
workers.finish()
