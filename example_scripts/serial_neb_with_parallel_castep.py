from ase.io import read
from ase.constraints import FixAtoms
from ase.calculators.emt import EMT
from ase.neb import NEB
from ase.optimize import BFGS
from dft_python_api.tasks.castep_task import DFT_Calculator, CastepTask

initial = read('initial.traj')
final = read('final.traj')

constraint = FixAtoms(mask=[atom.tag > 1 for atom in initial])

calc = DFT_Calculator(calculator_type=CastepTask, label=f"neb", debug=False, num_mpi_processes=2)

images = [initial]
for i in range(3):
    image = initial.copy()
    image.calc  =calc
    image.set_constraint(constraint)
    images.append(image)

images.append(final)

neb = NEB(images, parallel=False, allow_shared_calculator=True)
neb.interpolate()
qn = BFGS(neb, trajectory='neb.traj')
qn.run(fmax=0.05)
