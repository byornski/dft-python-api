#!/usr/bin/env python3
"""Main entry point for project"""
import atexit
from ase import Atoms
from ase.data import chemical_symbols

from dft_python_api import Worker, units
from dft_python_api.tasks.castep_task import CastepTask as dft

atom_no = 2
positions_min, positions_max, num_steps = 1.0*units.bohr, 4.0*units.bohr, 20

step_size = (positions_max - positions_min) / (num_steps-1)

# Generate some tasks to do
tasks = {}
for step in range(num_steps):
    x = positions_min + step * step_size

    cell_data = Atoms(
        "MgO",
        positions=[(0,0,0), (x.to(units.bohr).magnitude, 0, 0)],
        cell=[10,10,10]
    )

    param_data = dft.get_default_parameters(
        "energy", cell_data, cutoff_energy = "300 eV", max_scf_cycles=100
    )

    tasks[step] = (x, cell_data, param_data)

def run(update_method):
    # Spawn worker
    workers = Worker("main", 4)

    print(f"Testing {update_method}")
    
    # Run a set of calculations
    for task_id, (sep, atoms, params) in tasks.items():
        if task_id==0:
            # If this is the first task, initialise
            workers.add_task(task_id, dft(dft.init, atoms, params))
        else:
            # Otherwise, extrapolate
            workers.add_task(task_id, dft(update_method, atoms))

        # Calculate ground state energy
        workers.add_task(task_id, dft(dft.calculate))

        # Wait for these to finish. You can also do additional
        # work on the main script while you wait for the results
        task_id, result = workers.get_next_result()

        print(f"Step: {task_id} \tSeparation: {sep:.2f} \t Energy: {result.energy.to(units.eV)}")



    # Close workers
    workers.add_task(0, dft(dft.finalise))
    workers.get_next_finished()
    workers.finish()

from timeit import timeit

num_samples = 1

print(timeit('run(dft.update_atoms)', globals=globals(), number=num_samples))
print(timeit('run(dft.update_atoms_extrap)', globals=globals(), number=num_samples))
