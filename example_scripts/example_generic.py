#!/usr/bin/env python3
"""Main entry point for project"""
from dft_python_api import Worker
from dft_python_api.tasks.generic_task import GenericTask


# Generate some tasks to do
num_steps = 5
tasks = {}
for step in range(num_steps):
    a = step
    b = 4 * step
    tasks[step] = (a, b)



# Spawn worker
workers = Worker("generic-example", 1)

# Run a set of calculations
for task_id, (a, b) in tasks.items():

    # Send the numbers off to be added
    workers.add_task(task_id, GenericTask(GenericTask.add, a, b))

    # Wait for these to finish. You can also do additional
    # work on the main script while you wait for the results
    task_id, result = workers.get_next_result()

    print(f"Step: {task_id} \t The sum of {a} and {b} is {result}")



# Close workers
workers.finish()
